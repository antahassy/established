<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use \App\Models\M_users;
use \App\Models\M_meja;

class C_meja extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_id_group = DB::table('tb_user_group')->where('id_user', $sess_id)->first()->id_group;
        $akses = DB::table('tb_menu')
        ->select('tb_rel_group.akses')
        ->where('tb_menu.url', 'table')
        ->where('tb_menu.deleted_at', null)
        ->where('tb_rel_group.id_group', $sess_id_group)
        ->where('tb_rel_group.deleted_at', null)
        ->join('tb_rel_group', 'tb_menu.id', '=', 'tb_rel_group.id_menu')
        ->get();
        if(count($akses) != 0){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
            if (in_array('1', $akses_temp)){
                $data = M_meja::select(
                    'id',
                    'nama',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at'
                )
                ->where('deleted_at', null)
                ->orderBy('id', 'asc')
                ->get();
                foreach ($data as $row) {
                    if (in_array('3', $akses_temp) && in_array('4', $akses_temp)){
                        $row->action = '<button class="btn btn-sm btn-outline-success btn_edit" style="margin: 2.5px;">Edit</button>' . '<button class="btn btn-sm btn-outline-danger btn_delete" style="margin: 2.5px;">Hapus</button>';
                    }
                    if (! in_array('3', $akses_temp) && in_array('4', $akses_temp)){
                        $row->action = '<button class="btn btn-sm btn-outline-danger btn_delete" style="margin: 2.5px;">Hapus</button>';
                    }
                    if (in_array('3', $akses_temp) && ! in_array('4', $akses_temp)){
                        $row->action = '<button class="btn btn-sm btn-outline-success btn_edit" style="margin: 2.5px;">Edit</button>';
                    }
                    if (! in_array('3', $akses_temp) && ! in_array('4', $akses_temp)){
                        $row->action = '';
                    }
                }
                if($request->ajax()){
                    return datatables()->of($data)->addIndexColumn()->toJson();
                }
                return view('meja.index')->with('active_menu', 'Meja')->with('akses_menu', $akses_temp);
            }else{
                return view('unauthorized');
            }
        }else{
            return view('unauthorized');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $data = M_meja::create(
            [
                'nama'          => $request->meja,
                'created_by'    => $sess_username,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => null
            ]
        );
        if($data){
            return response()->json([
                'success'   => true,
                'type'      => 'disimpan'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = M_meja::find($id);
        return response()->json([
            'id'            => $data->id,
            'nama'          => $data->nama
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $data = M_meja::find($id);
        $data->nama         = $request->meja;
        $data->updated_by   = $sess_username;
        $data->updated_at   = date('Y-m-d H:i:s');
        $data->save();

        return response()->json([
            'success'   => true,
            'type'      => 'diupdate'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function hapus(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $id = $request->id;
        $data = M_meja::find($id);
        $data->deleted_by   = $sess_username;
        $data->deleted_at   = date('Y-m-d H:i:s');
        $data->save();

        return response()->json([
            'success'   => true
        ]);
    }
}
