<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use \App\Models\M_login;

class Empty_Session
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(! $request->session()->has('user.id')) {
            return $next($request);
        }
        $data = M_login::where("id", $request->session()->get('user.id'))->first();
        if($data)
        {
            return Redirect("dashboard");
            
        }
        return $next($request);
    }
}
