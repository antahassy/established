<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use \App\Models\M_users;
use \App\Models\M_food;

class C_food extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_id_group = DB::table('tb_user_group')->where('id_user', $sess_id)->first()->id_group;
        $akses = DB::table('tb_menu')
        ->select('tb_rel_group.akses')
        ->where('tb_menu.url', 'feed')
        ->where('tb_menu.deleted_at', null)
        ->where('tb_rel_group.id_group', $sess_id_group)
        ->where('tb_rel_group.deleted_at', null)
        ->join('tb_rel_group', 'tb_menu.id', '=', 'tb_rel_group.id_menu')
        ->get();
        if(count($akses) != 0){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
            if (in_array('1', $akses_temp)){
                $data = M_food::select(
                    'id',
                    'kategori',
                    'nama',
                    'image',
                    'harga',
                    'status',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at'
                )
                ->where('deleted_at', null)
                ->orderBy('id', 'asc')
                ->get();
                foreach ($data as $row) {
                    if (in_array('3', $akses_temp) && in_array('4', $akses_temp)){
                        $row->action = '<button class="btn btn-sm btn-outline-success btn_edit" style="margin: 2.5px;">Edit</button>' . '<button class="btn btn-sm btn-outline-danger btn_delete" style="margin: 2.5px;">Hapus</button>';
                    }
                    if (! in_array('3', $akses_temp) && in_array('4', $akses_temp)){
                        $row->action = '<button class="btn btn-sm btn-outline-danger btn_delete" style="margin: 2.5px;">Hapus</button>';
                    }
                    if (in_array('3', $akses_temp) && ! in_array('4', $akses_temp)){
                        $row->action = '<button class="btn btn-sm btn-outline-success btn_edit" style="margin: 2.5px;">Edit</button>';
                    }
                    if (! in_array('3', $akses_temp) && ! in_array('4', $akses_temp)){
                        $row->action = '';
                    }
                }
                if($request->ajax()){
                    return datatables()->of($data)->addIndexColumn()->toJson();
                }
                return view('food.index')->with('active_menu', 'Makanan & Minuman')->with('akses_menu', $akses_temp);
            }else{
                return view('unauthorized');
            }
        }else{
            return view('unauthorized');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $delete_berkas = $request->delete_berkas;
        $last_berkas = $request->get_berkas;

        $file_path = public_path('food');
        $file_extension = array('jpg', 'jpeg', 'png', 'bmp', 'gif', 'svg', 'ico');
        $file_size = 5120000;//5 mb
        $file_upload = $request->file('berkas');
        if($file_upload != ''){
            if($file_upload->getSize() > $file_size){
                return response()->json([
                    'size'   => true
                ]);
            }else{
                if (in_array($file_upload->guessClientExtension(), $file_extension)){
                    $berkas = time() . '-' . $file_upload->getClientOriginalName();
                    $request->berkas->move($file_path, $berkas);
                    if($last_berkas != ''){
                        File::delete($file_path . '/' . $last_berkas);
                    }elseif($delete_berkas != ''){
                        File::delete($file_path . '/' . $delete_berkas);
                    }
                }else{
                    return response()->json([
                        'extension'   => true
                    ]);
                }
            }
        }else{
            if($last_berkas == '' && $delete_berkas != ''){
                File::delete($file_path . '/' . $delete_berkas);
                $berkas  = '';
            }elseif($last_berkas == '' && $delete_berkas == ''){
                $berkas  = '';
            }else{
                $berkas  = $last_berkas;
            }
        }
        $data = M_food::create(
            [
                'kategori'      => $request->s_kategori,
                'nama'          => $request->nama,
                'image'         => $berkas,
                'harga'         => $request->harga,
                'created_by'    => $sess_username,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => null
            ]
        );
        if($data){
            return response()->json([
                'success'   => true,
                'type'      => 'disimpan'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = M_food::find($id);
        return response()->json([
            'id'        => $data->id,
            'kategori'  => $data->kategori,
            'nama'      => $data->nama,
            'harga'     => $data->harga,
            'image'     => $data->image
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $delete_berkas = $request->delete_berkas;
        $last_berkas = $request->get_berkas;

        $file_path = public_path('food');
        $file_extension = array('jpg', 'jpeg', 'png', 'bmp', 'gif', 'svg', 'ico');
        $file_size = 5120000;//5 mb
        $file_upload = $request->file('berkas');
        if($file_upload != ''){
            if($file_upload->getSize() > $file_size){
                return response()->json([
                    'size'   => true
                ]);
            }else{
                if (in_array($file_upload->guessClientExtension(), $file_extension)){
                    $berkas = time() . '-' . $file_upload->getClientOriginalName();
                    $request->berkas->move($file_path, $berkas);
                    if($last_berkas != ''){
                        File::delete($file_path . '/' . $last_berkas);
                    }elseif($delete_berkas != ''){
                        File::delete($file_path . '/' . $delete_berkas);
                    }
                }else{
                    return response()->json([
                        'extension'   => true
                    ]);
                }
            }
        }else{
            if($last_berkas == '' && $delete_berkas != ''){
                File::delete($file_path . '/' . $delete_berkas);
                $berkas  = '';
            }elseif($last_berkas == '' && $delete_berkas == ''){
                $berkas  = '';
            }else{
                $berkas  = $last_berkas;
            }
        }

        $data = M_food::find($id);
        $data->kategori     = $request->s_kategori;
        $data->nama         = $request->nama;
        $data->harga        = $request->harga;
        $data->image        = $berkas;
        $data->updated_by   = $sess_username;
        $data->updated_at   = date('Y-m-d H:i:s');
        $data->save();

        return response()->json([
            'success'   => true,
            'type'      => 'diupdate'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function hapus(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $id = $request->id;
        $data = M_food::find($id);
        $data->deleted_by   = $sess_username;
        $data->deleted_at   = date('Y-m-d H:i:s');
        $data->save();
        return response()->json([
            'success'   => true
        ]);
    }

    public function ready(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $id = $request->id;
        $data = M_food::find($id);
        $data->status       = '1';
        $data->updated_by   = $sess_username;
        $data->updated_at   = date('Y-m-d H:i:s');
        $data->save();
        return response()->json([
            'success'   => true
        ]);
    }

    public function non_ready(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $id = $request->id;
        $data = M_food::find($id);
        $data->status   = '0';
        $data->updated_by   = $sess_username;
        $data->updated_at   = date('Y-m-d H:i:s');
        $data->save();
        return response()->json([
            'success'   => true
        ]);
    }
}
