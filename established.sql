-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 22, 2021 at 05:59 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.3.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `established`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(49, '2021_03_19_062356_create_tb_user_table', 1),
(50, '2021_03_23_034830_create_tb_group_table', 1),
(51, '2021_03_23_034915_create_tb_menu_table', 1),
(52, '2021_03_23_034940_create_tb_rel_group_table', 1),
(53, '2021_03_23_035003_create_tb_user_group_table', 1),
(54, '2021_04_08_041250_create_tb_setting_table', 1),
(55, '2021_04_22_123323_create_tb_food_table', 1),
(56, '2021_04_22_123428_create_tb_order_table', 1),
(57, '2021_04_22_124255_create_tb_meja_table', 1),
(58, '2021_04_22_124438_create_tb_order_detail_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_food`
--

CREATE TABLE `tb_food` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kategori` smallint(6) NOT NULL DEFAULT 0,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `harga` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `deleted_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_group`
--

CREATE TABLE `tb_group` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `deleted_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_group`
--

INSERT INTO `tb_group` (`id`, `name`, `description`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', 'Administrator', 'System', '', '', '2021-04-22 11:40:42', NULL, NULL),
(2, 'waiter', 'Waiter', 'administrator', '', '', '2021-04-22 11:43:26', '2021-04-22 11:43:26', NULL),
(3, 'cashier', 'Cashier', 'administrator', '', '', '2021-04-22 11:43:35', '2021-04-22 11:43:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_meja`
--

CREATE TABLE `tb_meja` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `deleted_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_meja`
--

INSERT INTO `tb_meja` (`id`, `nama`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Meja 1', 'administrator', '', '', '2021-04-22 11:44:04', NULL, NULL),
(2, 'Meja 2', 'administrator', '', '', '2021-04-22 11:44:10', NULL, NULL),
(3, 'Meja 3', 'administrator', '', '', '2021-04-22 11:44:16', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_menu`
--

CREATE TABLE `tb_menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `menu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `urutan` smallint(6) NOT NULL DEFAULT 0,
  `rel` smallint(6) NOT NULL DEFAULT 0,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `deleted_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_menu`
--

INSERT INTO `tb_menu` (`id`, `menu`, `url`, `urutan`, `rel`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Dashboard', 'dashboard', 1, 0, 'System', '', '', '2021-04-22 11:40:42', NULL, NULL),
(2, 'Order', 'order', 2, 0, 'administrator', '', '', '2021-04-22 11:41:28', '2021-04-22 11:41:28', NULL),
(3, 'Menu', 'feed', 3, 0, 'administrator', '', '', '2021-04-22 11:41:41', '2021-04-22 11:41:50', NULL),
(4, 'Meja', 'table', 4, 0, 'administrator', '', '', '2021-04-22 11:42:02', '2021-04-22 11:42:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_order`
--

CREATE TABLE `tb_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_meja` int(11) NOT NULL DEFAULT 0,
  `order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` smallint(6) NOT NULL DEFAULT 0,
  `biaya` int(11) DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `deleted_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_order_detail`
--

CREATE TABLE `tb_order_detail` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_order` int(11) NOT NULL DEFAULT 0,
  `id_food` int(11) NOT NULL DEFAULT 0,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `deleted_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_rel_group`
--

CREATE TABLE `tb_rel_group` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_group` smallint(6) NOT NULL DEFAULT 0,
  `id_menu` smallint(6) NOT NULL DEFAULT 0,
  `akses` smallint(6) NOT NULL DEFAULT 0,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `deleted_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_rel_group`
--

INSERT INTO `tb_rel_group` (`id`, `id_group`, `id_menu`, `akses`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 'System', '', '', '2021-04-22 11:40:42', NULL, NULL),
(2, 1, 1, 2, 'System', '', 'administrator', '2021-04-22 11:40:42', NULL, '2021-04-22 15:37:29'),
(3, 1, 1, 3, 'System', '', 'administrator', '2021-04-22 11:40:42', NULL, '2021-04-22 15:37:29'),
(4, 1, 1, 4, 'System', '', 'administrator', '2021-04-22 11:40:42', NULL, '2021-04-22 15:37:29'),
(5, 1, 2, 1, 'administrator', '', '', '2021-04-22 11:43:51', NULL, NULL),
(6, 1, 2, 2, 'administrator', '', '', '2021-04-22 11:43:51', NULL, NULL),
(7, 1, 2, 3, 'administrator', '', '', '2021-04-22 11:43:51', NULL, NULL),
(8, 1, 2, 4, 'administrator', '', '', '2021-04-22 11:43:51', NULL, NULL),
(9, 1, 3, 1, 'administrator', '', '', '2021-04-22 11:43:51', NULL, NULL),
(10, 1, 3, 2, 'administrator', '', '', '2021-04-22 11:43:51', NULL, NULL),
(11, 1, 3, 3, 'administrator', '', '', '2021-04-22 11:43:52', NULL, NULL),
(12, 1, 3, 4, 'administrator', '', '', '2021-04-22 11:43:52', NULL, NULL),
(13, 1, 4, 1, 'administrator', '', '', '2021-04-22 11:43:52', NULL, NULL),
(14, 1, 4, 2, 'administrator', '', '', '2021-04-22 11:43:52', NULL, NULL),
(15, 1, 4, 3, 'administrator', '', '', '2021-04-22 11:43:52', NULL, NULL),
(16, 1, 4, 4, 'administrator', '', '', '2021-04-22 11:43:52', NULL, NULL),
(17, 2, 1, 1, 'administrator', '', '', '2021-04-22 15:39:23', NULL, NULL),
(18, 2, 1, 2, 'administrator', '', 'administrator', '2021-04-22 15:39:23', NULL, '2021-04-22 15:39:23'),
(19, 2, 1, 3, 'administrator', '', 'administrator', '2021-04-22 15:39:23', NULL, '2021-04-22 15:39:23'),
(20, 2, 1, 4, 'administrator', '', 'administrator', '2021-04-22 15:39:24', NULL, '2021-04-22 15:39:24'),
(21, 2, 2, 1, 'administrator', '', '', '2021-04-22 15:39:24', NULL, NULL),
(22, 2, 2, 2, 'administrator', '', '', '2021-04-22 15:39:24', NULL, NULL),
(23, 2, 2, 3, 'administrator', '', '', '2021-04-22 15:39:24', NULL, NULL),
(24, 2, 2, 4, 'administrator', '', 'administrator', '2021-04-22 15:39:24', NULL, '2021-04-22 15:39:24'),
(25, 2, 3, 1, 'administrator', '', '', '2021-04-22 15:39:24', NULL, NULL),
(26, 2, 3, 2, 'administrator', '', '', '2021-04-22 15:39:24', NULL, NULL),
(27, 2, 3, 3, 'administrator', '', '', '2021-04-22 15:39:24', NULL, NULL),
(28, 2, 3, 4, 'administrator', '', '', '2021-04-22 15:39:24', NULL, NULL),
(29, 2, 4, 1, 'administrator', '', '', '2021-04-22 15:39:24', NULL, NULL),
(30, 2, 4, 2, 'administrator', '', '', '2021-04-22 15:39:24', NULL, NULL),
(31, 2, 4, 3, 'administrator', '', '', '2021-04-22 15:39:24', NULL, NULL),
(32, 2, 4, 4, 'administrator', '', '', '2021-04-22 15:39:24', NULL, NULL),
(33, 3, 1, 1, 'administrator', '', '', '2021-04-22 15:39:58', NULL, NULL),
(34, 3, 1, 2, 'administrator', '', 'administrator', '2021-04-22 15:39:59', NULL, '2021-04-22 15:39:59'),
(35, 3, 1, 3, 'administrator', '', 'administrator', '2021-04-22 15:39:59', NULL, '2021-04-22 15:39:59'),
(36, 3, 1, 4, 'administrator', '', 'administrator', '2021-04-22 15:39:59', NULL, '2021-04-22 15:39:59'),
(37, 3, 2, 1, 'administrator', '', '', '2021-04-22 15:39:59', NULL, NULL),
(38, 3, 2, 2, 'administrator', '', 'administrator', '2021-04-22 15:39:59', NULL, '2021-04-22 15:39:59'),
(39, 3, 2, 3, 'administrator', '', '', '2021-04-22 15:39:59', NULL, NULL),
(40, 3, 2, 4, 'administrator', '', 'administrator', '2021-04-22 15:39:59', NULL, '2021-04-22 15:39:59'),
(41, 3, 3, 1, 'administrator', '', '', '2021-04-22 15:39:59', NULL, NULL),
(42, 3, 3, 2, 'administrator', '', '', '2021-04-22 15:39:59', NULL, NULL),
(43, 3, 3, 3, 'administrator', '', '', '2021-04-22 15:39:59', NULL, NULL),
(44, 3, 3, 4, 'administrator', '', '', '2021-04-22 15:39:59', NULL, NULL),
(45, 3, 4, 1, 'administrator', '', '', '2021-04-22 15:39:59', NULL, NULL),
(46, 3, 4, 2, 'administrator', '', '', '2021-04-22 15:39:59', NULL, NULL),
(47, 3, 4, 3, 'administrator', '', '', '2021-04-22 15:39:59', NULL, NULL),
(48, 3, 4, 4, 'administrator', '', '', '2021-04-22 15:39:59', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_setting`
--

CREATE TABLE `tb_setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `meta_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `updated_at` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_setting`
--

INSERT INTO `tb_setting` (`id`, `meta_id`, `value`, `updated_by`, `updated_at`) VALUES
(1, 'Judul', 'Established Job Test', 'administrator', '2021-04-22 18:41:10'),
(2, 'Deskripsi', 'Antahassy Wibawa', 'administrator', '2021-04-22 18:41:10'),
(3, 'Logo', '1619091670-es(1).png', 'administrator', '2021-04-22 18:41:10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` longtext COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `active` smallint(6) NOT NULL DEFAULT 0,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `deleted_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `password`, `active`, `email`, `phone`, `image`, `nama`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'administrator', '$2y$10$Xi/fGhlKD68IHzVOtmgC5eXgWrqa6o0rqPF/ssgfENX26u6TfMhI.', 1, 'antahassyw@gmail.com', '087770067003', '', 'administrator', 'System', 'administrator', '', '2021-04-22 11:40:42', '2021-04-22 15:43:11', NULL),
(2, 'established1', '$2y$10$MwI9NLq4ILMxO4umRrQp5.KamXOBubog9/7ieuBAf2/H8Cx9.22Ge', 1, NULL, NULL, '', 'established1', 'administrator', '', '', '2021-04-22 15:41:02', '2021-04-22 15:41:02', NULL),
(3, 'established2', '$2y$10$O2Y/vDP9nGTgm7BqFGjim.MdKYbCHsg25fa7UrG4BeIWI2YN/4WvG', 1, NULL, NULL, '', 'established2', 'administrator', '', '', '2021-04-22 15:41:20', '2021-04-22 15:41:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user_group`
--

CREATE TABLE `tb_user_group` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_user` smallint(6) NOT NULL DEFAULT 0,
  `id_group` smallint(6) NOT NULL DEFAULT 0,
  `created_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `updated_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `deleted_by` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_user_group`
--

INSERT INTO `tb_user_group` (`id`, `id_user`, `id_group`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'System', 'administrator', '', '2021-04-22 11:40:42', '2021-04-22 15:43:11', NULL),
(2, 2, 2, 'administrator', '', '', '2021-04-22 15:41:02', NULL, NULL),
(3, 3, 3, 'administrator', '', '', '2021-04-22 15:41:20', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_food`
--
ALTER TABLE `tb_food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_group`
--
ALTER TABLE `tb_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_meja`
--
ALTER TABLE `tb_meja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_menu`
--
ALTER TABLE `tb_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_order`
--
ALTER TABLE `tb_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_rel_group`
--
ALTER TABLE `tb_rel_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_setting`
--
ALTER TABLE `tb_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user_group`
--
ALTER TABLE `tb_user_group`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `tb_food`
--
ALTER TABLE `tb_food`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_group`
--
ALTER TABLE `tb_group`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_meja`
--
ALTER TABLE `tb_meja`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_menu`
--
ALTER TABLE `tb_menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_order`
--
ALTER TABLE `tb_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_order_detail`
--
ALTER TABLE `tb_order_detail`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_rel_group`
--
ALTER TABLE `tb_rel_group`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `tb_setting`
--
ALTER TABLE `tb_setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_user_group`
--
ALTER TABLE `tb_user_group`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
