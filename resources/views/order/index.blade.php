@extends('layout/dashboard')
@section('content')
<style>
    .thead-dark tr th{
        color: #fff !important;
    }
    form label{
        margin-top: 5px;
        font-weight: 600 !important;
    }
</style>
<?php
    $sess_id = Session::get('user.id');
    $session = DB::table('tb_user')->where('id', $sess_id)->first();
    $id_group = DB::table('tb_user_group')->where('id_user', $sess_id)->first()->id_group;
    $level_group = DB::table('tb_group')->where('id', $id_group)->first();
?>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <?php
                    if(in_array('2', $akses_menu)){
                        echo '<h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">' . $active_menu . '</h5>';
                        echo '<div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>';
                        echo '<a href="#" class="btn btn-light-success font-weight-bolder btn-sm" id="btn_add">Tambah Data</a>';
                    }else{
                        echo '<h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">' . $active_menu . '</h5>';
                    }
                ?>
            </div>
            <div class="d-flex align-items-center">
                <!-- <a href="#" id="d_daily" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Daily</a>
                <a href="#" id="d_monthly" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Monthly</a>
                <a href="#" id="d_yearly" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Yearly</a>
                <a href="#" class="btn btn-sm btn-light font-weight-bold mr-2" id="kt_dashboard_daterangepicker" data-toggle="tooltip" title="Select Daterange" data-placement="left">
                    <span class="text-muted font-size-base font-weight-bold mr-2" id="kt_dashboard_daterangepicker_title"></span>
                    <span class="text-primary font-size-base font-weight-bolder" id="kt_dashboard_daterangepicker_date"></span>
                </a> -->
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row" style="min-height: 68vh;">
                <div class="col-lg-12 col-xxl-4 order-1 order-xxl-2">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark">Daftar {{ $active_menu }}</h3>
                        </div>
                        <div class="card-body pt-0">
                            <table id="table_data" class="table table-striped table-vcenter" style="width: 100%;">
                                <thead class="thead-dark">
                                    <tr> 
                                        <th>No</th>
                                        <th>Meja</th>
                                        <th>Order</th>
                                        <th>Menu</th>
                                        <th>Total</th>
                                        <th>Status</th>
                                        <th>Tindakan</th> 
                                        <th>Dibuat</th> 
                                        <th>Diupdate</th> 
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
            </div>
            <div class="modal-body"> 
                <form id="form_data">
                    <input type="hidden" id="id_data" name="id_data">
                    <input type="hidden" name="_method" id="_method">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Meja<span style="color: red;">*</span></label>
                            <select class="form-control" name="s_meja" id="s_meja">
                                <option value="">Pilih Meja</option>
                            </select>
                            <label>Menu<span style="color: red;">*</span></label>
                            <table id="table_s_food_menu" class="table table-striped table-vcenter" style="width: 100%;">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Pilih</th>
                                        <th>Menu</th>
                                        <th>Harga</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody_s_food_menu"></tbody>
                            </table>
                            <label>Total Biaya</label>
                            <input type="hidden" name="total_hidden" id="total_hidden">
                            <input type="text" name="total" id="total" readonly="" style="background-color: #3F4254; color: #fff;" class="form-control">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-outline-primary" id="btn_process"></button>
            </div>
        </div>
    </div>
</div>
<script>
    var level_position = '<?php echo $level_group->description ?>';
</script>
<script>
	$(document).ready(function(){
        var sum = (index, value) => index + value;
        $.ajaxSetup({
            headers     : {
                'X-CSRF-TOKEN'  : $('meta[name="csrf-token"]').attr('content')
            }
        });
        var nama_bulan = new Array();
        nama_bulan[1] = 'Januari';
        nama_bulan[2] = 'Februari';
        nama_bulan[3] = 'Maret';
        nama_bulan[4] = 'April';
        nama_bulan[5] = 'Mei';
        nama_bulan[6] = 'Juni';
        nama_bulan[7] = 'Juli';
        nama_bulan[8] = 'Agustus'; 
        nama_bulan[9] = 'September'; 
        nama_bulan[10] = 'Oktober';
        nama_bulan[11] = 'November'; 
        nama_bulan[12] = 'Desember';
        var table_data = '';
        var arr_menu = [];
        swal({
            showConfirmButton   : false,
            allowOutsideClick   : false,
            allowEscapeKey      : false,
            background          : 'transparent',
            onOpen  : function(){
                swal.showLoading();
                setTimeout(function(){
                    data_table();
                },500);
            }
        });
        function s_table(table_val){
            $.ajax({
                type        : 'ajax',
                method      : 'post',
                url         : site + '/order/s_meja',
                dataType    : "json",
                async       : false,
                success: function(meja_data){
                    if(meja_data.length == 0){
                        swal({
                            background  : 'transparent',
                            html        : '<pre>Meja belum tersedia</pre>',
                            type        : "warning"
                        });
                    }else{
                        var s_meja = '<option value="">Pilih Meja</option>';
                        for(i = 0; i < meja_data.length; i ++){
                            s_meja += '<option value="' + meja_data[i].id + '">' + meja_data[i].nama + '</option>';
                        }
                        $('#s_meja').html(s_meja);
                        if(table_val != ''){
                            $('#s_meja').val(table_val);
                        }else{
                            $('#s_meja').val('');
                        }
                        s_food();
                    }
                },
                error: function (){
                    swal({
                        background  : 'transparent',
                        html        : '<pre>Koneksi terputus' + '<br>' + 
                                      'Cobalah beberapa saat lagi</pre>',
                        type        : "warning"
                    });
                }
            });
        }
        function s_food(){
            $.ajax({
                type        : 'ajax',
                method      : 'post',
                url         : site + '/order/s_menu',
                dataType    : "json",
                async       : false,
                success        : function(food_data){
                    if(food_data.length == 0){
                        swal({
                            background  : 'transparent',
                            html        : '<pre>Menu belum tersedia</pre>',
                            type        : "warning"
                        });
                    }else{
                        var s_menu = '';
                        if($('#form_data').attr('action') == site + '/order'){
                            arr_menu = [];
                        }
                        for(i = 0; i < food_data.length; i ++){
                            var nama_menu = food_data[i].nama;
                            var harga_menu = Number(food_data[i].harga.replace(".", ""));
                            if(food_data[i].image != null && food_data[i].image != ''){
                                nama_menu = '<img align="center" src="' + site + '/food/' + food_data[i].image + '" style="width: 125px; height: 75px;">' + '<br>' + '<div style="padding-top: 5px; font-weight: 800;">' + food_data[i].nama + '</div>'
                            }
                            var attr_check = '';
                            for(x = 0; x < arr_menu.length; x ++){
                                if(arr_menu[x].id == food_data[i].id){
                                    attr_check = 'checked=""';
                                }
                            }
                            s_menu +=
                            '<tr>' +
                                '<td style="vertical-align: middle;">' +
                                    '<label class="checkbox checkbox-success">' +
                                    '<input type="checkbox" class="input_checkbox" name="' + food_data[i].id + '" id="' + food_data[i].id + '" alt="' + harga_menu + '" ' + attr_check + '/>' +
                                    '<span style="border: 2px solid #1bc5bd; border-radius: 0;"></span></label>' +
                                '</td>' +
                                '<td>' + nama_menu + '</td>' +
                                '<td style="vertical-align: middle;">Rp. ' + harga_menu.toLocaleString('de') + '</td>' +
                            '</tr>';
                        }
                        $('#tbody_s_food_menu').html(s_menu);
                        

                        swal.close();
                        $('#modal_form').modal('show');
                        data_process();
                    }
                },
                error   : function(){
                    swal({
                        background  : 'transparent',
                        html        : '<pre>Connection Lost' + '<br>' + 
                                      'Please try again later</pre>',
                        type        : "warning"
                    });
                }
            });
        }
        function main(){
            $('#tbody_s_food_menu').on('click', '.input_checkbox', function(){
                var id_menu = $(this).attr('id');
                var menu_harga = $(this).attr('alt');
                if(! $(this).is(':checked')){
                    arr_menu = arr_menu.filter(function(item) {
                        return item.id !== id_menu;
                    })
                }else{
                    arr_menu.push({
                        id      : id_menu,
                        price   : Number(menu_harga)
                    });
                }
                arr_menu.sort(function(a, b) {
                    var id_a = a.id;
                    var id_b = b.id;
                    if (id_a < id_b) {
                         return -1;
                    }
                    if (id_a > id_b) {
                         return 1;
                    }
                    return 0;
                });
                console.log(arr_menu)
                
                var arr_total = arr_menu.map(x => x.price);
                var total_akhir = '';
                if(arr_total.length != 0){
                    total_akhir = 'Rp. ' + Number(arr_total.reduce(sum)).toLocaleString('de');
                }
                $('#total').val(total_akhir);
                $('#total_hidden').val(Number(arr_total.reduce(sum)));
            });
            var modal_form;
            $('#modal_form').on('show.bs.modal', function(){
                $(this).addClass('zoomIn');
                modal_form = true;
            });
            $('#modal_form').on('hide.bs.modal', function(){
                if(modal_form){
                    $(this).removeClass('zoomIn').addClass('zoomOut');
                    modal_form = false;
                    setTimeout(function(){
                        $('#modal_form').modal('hide');
                    },350);
                    return false;
                }
                $(this).removeClass('zoomOut');
            });
            $('#btn_add').on('click', function(){
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $('#form_data')[0].reset();
                            $('#modal_form').find('.modal-title').text('Tambah');
                            $('#btn_process').text('Simpan');
                            $('#form_data').attr('method', 'post');
                            $('#_method').val('post');
                            $('#form_data').attr('action', site + '/order');
                            var table_val = '', arr_menu = [];
                            s_table(table_val);
                        },500);
                    }
                });
            });
            $('#table_data').on('click', '.btn_edit', function(){
                var action_data = table_data.row($(this).parents('tr')).data();
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : 'get',
                                url            : site + '/order/' + action_data.id + '/edit',
                                async          : true,
                                dataType       : 'json',
                                success        : function(data){
                                    $('#form_data')[0].reset();
                                    $('#total').val('Rp. ' + Number(data.biaya).toLocaleString('de'));
                                    $('#total_hidden').val(data.biaya);
                                    $('#modal_form').find('.modal-title').text('Edit Order ' + data.order);
                                    $('#btn_process').text('Update');
                                    $('#form_data').attr('method', 'put');
                                    $('#_method').val('put');
                                    $('#form_data').attr('action', site + '/order/' + data.id);
                                    arr_menu = [];
                                    for(i = 0; i < data.detail.length; i ++){
                                        arr_menu.push({
                                            id      : data.detail[i].id,
                                            price   : data.detail[i].price
                                        });
                                    }

                                    var table_val = '';
                                    if(data.id_meja != '0'){
                                        table_val = data.id_meja;
                                    }
                                    s_table(table_val);
                                },
                                error   : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Koneksi terputus' + '<br>' + 
                                                      'Cobalah beberapa saat lagi</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });     
            });
            $('#table_data').on('click', '.btn_delete', function(){
                var action_data = table_data.row($(this).parents('tr')).data();
                swal({
                    html                : '<pre>Hapus data ini ?</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'Tidak',
                    confirmButtonText   : 'Ya'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        data        : {id : action_data.id},
                                        url         : site + '/order/hapus',
                                        dataType    : "json",
                                        async       : true,
                                        success   : function(response){
                                            if(response.success){
                                                swal({
                                                    html                : '<pre>Data berhasil dihapus</pre>',
                                                    type                : "success",
                                                    background          : 'transparent',
                                                    allowOutsideClick   : false,
                                                    allowEscapeKey      : false, 
                                                    showConfirmButton   : false,
                                                    timer               : 1000
                                                }).then(function(){
                                                    setTimeout(function(){
                                                        table_data.ajax.reload();
                                                    },500);
                                                });
                                            }
                                        },
                                        error          : function(){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Koneksi terputus' + '<br>' +
                                                              'Cobalah beberapa saat lagi</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            });
            $('#table_data').on('click', '.btn_close', function(){
                var id = $(this).attr('id');
                swal({
                    html                : '<pre>Tutup order ?</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'Tidak',
                    confirmButtonText   : 'Ya'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        data        : {id : id},
                                        url         : site + '/order/close_order',
                                        dataType    : "json",
                                        async       : true,
                                        success: function(response){
                                            if(response.success){
                                                swal.close();
                                                setTimeout(function(){
                                                    table_data.ajax.reload();
                                                },500);
                                            }
                                        },
                                        error: function (){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Koneksi terputus' + '<br>' + 
                                                              'Cobalah beberapa saat lagi</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            });
        }
        function data_process(){
            $('#btn_process').on('click', function(event){
                event.preventDefault();
                event.stopImmediatePropagation();
                var ajax_method     = $('#form_data').attr('method');
                var ajax_url        = $('#form_data').attr('action');
                var form_data       = $('#form_data')[0];
                form_data           = new FormData(form_data);
                var errormessage    = '';
                if(! $('#s_meja').val()){
                     errormessage += 'Meja dibutuhkan \n';
                }
                if(arr_menu.length == 0){
                     errormessage += 'Harap pilih salah satu menu \n';
                }
                if(errormessage !== ''){
                    swal({
                        background  : 'transparent',
                        html        : '<pre>' + errormessage + '</pre>'
                    });
                }else{
                    swal({
                        background          : 'transparent',
                        html                : '<pre>Apakah data sudah benar ?</pre>',
                        type                : 'question',
                        showCancelButton    : true,
                        cancelButtonText    : 'Tidak',
                        confirmButtonText   : 'Ya'
                    }).then((result) => {
                        if(result.value){
                            swal({
                                showConfirmButton   : false,
                                allowOutsideClick   : false,
                                allowEscapeKey      : false,
                                background          : 'transparent',
                                onOpen  : function(){
                                    swal.showLoading();
                                    setTimeout(function(){
                                        $.ajax({
                                            type           : 'ajax',
                                            method         : ajax_method,
                                            url            : ajax_url,
                                            data           : {
                                                id_meja     : $('#s_meja').val(),
                                                total       : $('#total_hidden').val(),
                                                list        : arr_menu
                                            },
                                            async          : true,
                                            // processData    : false,
                                            // contentType    : false,
                                            // cache          : false,
                                            dataType       : 'json',
                                            success        : function(response){
                                                if(response.success){
                                                    $('#modal_form').modal('hide');
                                                    $('#form_data')[0].reset();
                                                    swal({
                                                        html                : '<pre>Data berhasil ' + response.type + '</pre>',
                                                        type                : "success",
                                                        background          : 'transparent',
                                                        allowOutsideClick   : false,
                                                        allowEscapeKey      : false, 
                                                        showConfirmButton   : false,
                                                        timer               : 1000
                                                    }).then(function(){
                                                        setTimeout(function(){
                                                            table_data.ajax.reload();
                                                            arr_menu = [];
                                                        },500);
                                                    });
                                                }
                                            },
                                            error   : function(){
                                                swal({
                                                    background  : 'transparent',
                                                    html        : '<pre>Koneksi terputus' + '<br>' + 
                                                                  'Cobalah beberapa saat lagi</pre>',
                                                    type        : "warning"
                                                });
                                            }
                                        });
                                    },500);
                                }
                            });     
                        }
                    });
                }
                return false;
            });
        }
        function data_table(){
            table_data = $('#table_data').DataTable({
                lengthMenu          : [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
                processing          : true, 
                destroy             : true,
                serverSide          : true, 
                scrollX             : true,
                scrollCollapse      : true,
                fixedColumns        : true, 
                initComplete: function(){
                    swal.close();
                    main();
                },
                ajax            : {
                    url         : site + '/order',
                    method      : 'get'
                },
                columns             : [
                    {data   : 'DT_RowIndex'},
                    {data   : 'meja'},
                    {data   : 'order'},
                    {data   : 'menu',
                        render: function(menu){
                            if(menu.length == 0){
                                return '';
                            }else{
                                var list_menu = menu.map(x => x.nama);
                                list_menu = list_menu.join('<br>');
                                return list_menu;
                            }
                        }
                    },
                    {data   : 'biaya',
                        render: function(biaya){
                            return 'Rp. ' + Number(biaya).toLocaleString('de');
                        }
                    },
                    {data   : 'status',
                        render  : (data, type, row) => {
                            if(row.status == '1'){
                                if(level_position == 'Waiter'){
                                    return '<div style="color: #3699FF;">Active</div>';
                                }else{
                                    return '<button class="btn btn-sm btn-outline-primary btn_close" id="' + row.id + '" style="margin: 2.5px;">Active</button>';
                                }
                            }else{
                                return '<div style="color: red;">Closed</div>';
                            }
                        }
                    },
                    {data   : 'action'},
                    {data   : 'created_at',
                        render: function(created_at){
                            var time = created_at.split(' ');
                            return time[0].split('-')[2] + '/' + nama_bulan[Number(time[0].split('-')[1])] + '/' + time[0].split('-')[0] + '<br>' + time[1]; 
                        }
                    },
                    {data   : 'updated_at',
                        render: function(updated_at){
                            if(updated_at != null){
                                var time = updated_at.split(' ');
                                return time[0].split('-')[2] + '/' + nama_bulan[Number(time[0].split('-')[1])] + '/' + time[0].split('-')[0] + '<br>' + time[1]; 
                            }else{
                                return '';
                            }
                        }
                    }
                ]
            });
        }
    })
</script>
@endsection