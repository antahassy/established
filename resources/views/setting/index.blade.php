 @extends('layout/dashboard')
@section('content')
<style>
    .thead-dark tr th{
        color: #fff !important;
    }
    form label{
        margin-top: 5px;
        font-weight: 600 !important;
    }
</style>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">{{ $active_menu }}</h5>
            </div>
            <div class="d-flex align-items-center">
                <!-- <a href="#" id="d_daily" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Daily</a>
                <a href="#" id="d_monthly" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Monthly</a>
                <a href="#" id="d_yearly" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Yearly</a>
                <a href="#" class="btn btn-sm btn-light font-weight-bold mr-2" id="kt_dashboard_daterangepicker" data-toggle="tooltip" title="Select Daterange" data-placement="left">
                    <span class="text-muted font-size-base font-weight-bold mr-2" id="kt_dashboard_daterangepicker_title"></span>
                    <span class="text-primary font-size-base font-weight-bolder" id="kt_dashboard_daterangepicker_date"></span>
                </a> -->
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row" style="min-height: 68vh;">
                <div class="col-lg-12 col-xxl-4 order-1 order-xxl-2">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark">Daftar {{ $active_menu }}</h3>
                        </div>
                        <div class="card-body pt-0">
                        	<form id="form_data">
                        		<input type="hidden" name="_method" id="_method">
                        		<div class="row" id="list_form">
	                        		
	                        	</div>
	                        	<div class="text-center">
	                        		<button type="button" id="btn_process" class="btn btn-light-success font-weight-bolder btn-sm" style="display: none;">Update</button>
	                        	</div>
                        	</form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready(function(){
		$.ajaxSetup({
			headers 	: {
				'X-CSRF-TOKEN' 	: $('meta[name="csrf-token"]').attr('content')
			}
		});
		var nama_bulan = new Array();
        nama_bulan[1] = 'Januari';
        nama_bulan[2] = 'Februari';
        nama_bulan[3] = 'Maret';
        nama_bulan[4] = 'April';
        nama_bulan[5] = 'Mei';
        nama_bulan[6] = 'Juni';
        nama_bulan[7] = 'Juli';
        nama_bulan[8] = 'Agustus'; 
        nama_bulan[9] = 'September'; 
        nama_bulan[10] = 'Oktober';
        nama_bulan[11] = 'November'; 
        nama_bulan[12] = 'Desember';
        var table_data = '';
		swal({
            showConfirmButton   : false,
            allowOutsideClick   : false,
            allowEscapeKey      : false,
            background          : 'transparent',
            onOpen  : function(){
                swal.showLoading();
                setTimeout(function(){
                    data_table();
                },500);
            }
        });
        function data_table(){
            $.ajax({
                type        : 'ajax',
                method      : 'get',
                url         : site + '/setting',
                dataType    : "json",
                async       : true,
                success: function(data){
                	var list_form = '';
                	for(i = 0; i < data.length; i ++){
                		if(i == 0){
                			list_form +=
	                		'<div class="col-lg-6 mb-5">' +
	                    		'<label>' + data[i].meta_id + ' Web<span class="text-danger">*</span></label>' +
	                    	'</div>' +
	                		'<div class="col-lg-6 mb-5">' +
	                			'<input type="text" name="' + data[i].meta_id + '" id="' + data[i].meta_id + '" class="form-control" value="' + data[i].value + '" />' +
	                		'</div>';
                		}
                		if(i == 1){
                			list_form +=
	                		'<div class="col-lg-6 mb-5">' +
	                    		'<label>' + data[i].meta_id + ' Web<span class="text-danger">*</span></label>' +
	                    	'</div>' +
	                		'<div class="col-lg-6 mb-5">' +
	                			'<textarea name="' + data[i].meta_id + '" id="' + data[i].meta_id + '" rows="5" class="form-control" style="width: 100%;">' + data[i].value + '</textarea>' +
	                		'</div>';
                		}
                		if(i == 2){
                			var logo_path = '', logo_image = '';
                			if(data[i].value != ''){
                				logo_path = site + '/logo/' + data[i].value;
                				logo_image = data[i].value;
                			}else{
                				logo_path = site + '/metronic/media/logos/favicon.ico';
                				logo_image = '';
                			}
                			list_form +=
	                		'<div class="col-lg-6 mb-5">' +
	                    		'<label>' + data[i].meta_id + ' Web<span class="text-danger">*</span></label>' +
	                    	'</div>' +
	                		'<div class="col-lg-6 mb-5">' +
		                		'<input type="hidden" name="delete_berkas" id="delete_berkas" value="' + logo_image + '">' +
	                            '<input type="hidden" name="get_berkas" id="get_berkas" value="' + logo_image + '">' +
	                            '<input type="file" name="berkas" id="berkas" value="" accept="image/*" style="width: 100%;">' +
	                            '<div>' +
	                                '<div id="delete_preview_items" style="display: block;">Hapus Gambar</div>' +
	                                '<img id="preview_items" src="' + logo_path + '" title="' + data[i].value + '">' +
	                            '</div>' +
	                		'</div>';
                		}
                	}
                	$('#list_form').html(list_form);
                    setTimeout(function(){
                    	$('#btn_process').show();
                    	$('#form_data').attr('method', 'post');
			            $('#_method').val('post');
			            $('#form_data').attr('action', site + '/setting');
                    	display_image();
                    	data_process();
                    	swal.close();
                    },500);
                },
                error: function (){
                    swal({
                        background  : 'transparent',
                        html        : '<pre>Koneksi terputus' + '<br>' + 
                                      'Cobalah beberapa saat lagi</pre>',
                        type        : "warning"
                    });
                }
            });
        }
        function display_image(){
            function preview(image){
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_items').attr('src', event.target.result);
                        $('#preview_items').attr('title', image.files[0].name);
                        $('#delete_preview_items').css('display','block');
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
            $("#berkas").on('change', function(){
                preview(this);
                var names = $(this).val();
                var file_names = names.replace(/^.*\\/, "");
            });
            $('#delete_preview_items').on('click', function(){
                $('#delete_preview_items').css('display','none');
                $('#preview_items').attr('src', '');
                $('#berkas, #get_berkas').val('');
            });
        }
        function data_process(){
         	$('#btn_process').on('click', function(event){
				event.preventDefault();
	            event.stopImmediatePropagation();
	            var ajax_method		= $('#form_data').attr('method');
	            var ajax_url   		= $('#form_data').attr('action');
	            var form_data       = $('#form_data')[0];
	            form_data       	= new FormData(form_data);
	            var errormessage    = '';
	            if(! $('#Judul').val()){
                    errormessage += 'Judul web dibutuhkan \n';
                }
                if(! $('#Deskripsi').val()){
                    errormessage += 'Deskripsi web dibutuhkan \n';
                }
	            if(errormessage !== ''){
	            	swal({
	                    background  : 'transparent',
	                    html        : '<pre>' + errormessage + '</pre>'
	                });
	            }else{
	                swal({
	                    background          : 'transparent',
	                    html                : '<pre>Apakah data sudah benar ?</pre>',
	                    type                : 'question',
	                    showCancelButton    : true,
	                    cancelButtonText    : 'Tidak',
	                    confirmButtonText   : 'Ya'
	                }).then((result) => {
	                    if(result.value){
	                        swal({
	                            showConfirmButton   : false,
	                            allowOutsideClick   : false,
	                            allowEscapeKey      : false,
	                            background          : 'transparent',
	                            onOpen  : function(){
	                                swal.showLoading();
	                                setTimeout(function(){
	                                    $.ajax({
	                                        type           : 'ajax',
	                                        method         : ajax_method,
	                                        url            : ajax_url,
	                                        data           : form_data,
	                                        async          : true,
	                                        processData    : false,
	                                        contentType    : false,
	                                        cache          : false,
	                                        dataType       : 'json',
	                                        success        : function(response){
	                                            if(response.success){
	                                                swal({
			                                            html                : '<pre>Data berhasil diupdate</pre>',
			                                            type                : "success",
			                                            background          : 'transparent',
			                                            allowOutsideClick   : false,
			                                            allowEscapeKey      : false, 
			                                            showConfirmButton   : false,
			                                            timer               : 1000
			                                        }).then(function(){
	                                                    setTimeout(function(){
                                                            location.reload(true);
                                                        },500);
	                                                });
	                                            }
	                                            if(response.size){
	                                                swal({
			                                            html                : '<pre>Ukuran file terlalu tinggi' + '<br>' + 
			                                            					  'Maks 5 mb</pre>',
			                                            type                : "warning",
			                                            background          : 'transparent'
			                                        });
	                                            }
	                                            if(response.extension){
	                                                swal({
			                                            html                : '<pre>Ekstensi file tidak sesuai</pre>',
			                                            type                : "warning",
			                                            background          : 'transparent'
			                                        });
	                                            }
	                                        },
	                                        error   : function(){
	                                            swal({
	                                                background  : 'transparent',
	                                                html        : '<pre>Koneksi terputus' + '<br>' + 
	                                                              'Cobalah beberapa saat lagi</pre>',
	                                                type        : "warning"
	                                            });
	                                        }
	                                    });
	                                },500);
	                            }
	                        });     
	                    }
	                });
	            }
	            return false;
			});   
        }
	});
</script>
@endsection