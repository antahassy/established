<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class tb_rel_group_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //multiple insert sesuai jumlah range data
    	// $faker = Faker::create();
    	foreach (range(1,4) as $row) {
    		DB::table('tb_rel_group')->insert([
	        	'id_group' 			=> '1',
	        	'id_menu' 			=> '1',
	        	'akses'		 		=> $row,
	        	'created_by'		=> 'System',
        		'created_at'		=> date('Y-m-d H:i:s'),
	        ]);
    	}

    	//single insert
        // DB::table('tb_rel_group')->insert([
        // 	'id_group' 			=> $row,
        // 	'id_menu' 			=> $row,
        // 	'akses'		 		=> $row,
        // 	'created_by'		=> 'System',
        // 	'created_at'		=> date('Y-m-d H:i:s'),
        // ]);
    }
}
