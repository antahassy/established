<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use \App\Models\M_users;
use \App\Models\M_menu;

class C_menu extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_id_group = DB::table('tb_user_group')->where('id_user', $sess_id)->first()->id_group;
        if($sess_id_group == '1'){
            $object = array();
            $data = M_menu::select(
                'id',
                'menu',
                'url',
                'urutan',
                'rel',
                'created_by',
                'created_at',
                'updated_by',
                'updated_at'
            )
            ->where('rel', '0')
            ->orderBy('urutan', 'asc')
            ->get();
            foreach ($data as $row) {
                $arr_row = array(
                    'id'            => $row->id,
                    'menu'          => $row->menu,
                    'url'           => $row->url,
                    'urutan'        => $row->urutan,
                    'rel'           => $row->rel,
                    'created_by'    => $row->created_by,
                    'created_at'    => $row->created_at,
                    'updated_by'    => $row->updated_by,
                    'updated_at'    => $row->updated_at
                );
                array_push($object, $arr_row);
                $sub_data = M_menu::select(
                    'id',
                    'menu',
                    'url',
                    'urutan',
                    'rel',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at'
                )
                ->where('rel', $row->id)
                ->orderBy('urutan', 'asc')
                ->get();
                if(count($sub_data) != 0){
                    foreach ($sub_data as $sub_row) {
                        $arr_sub_row = array(
                            'id'            => $sub_row->id,
                            'menu'          => $sub_row->menu,
                            'url'           => $sub_row->url,
                            'urutan'        => $sub_row->urutan,
                            'rel'           => $sub_row->rel,
                            'created_by'    => $sub_row->created_by,
                            'created_at'    => $sub_row->created_at,
                            'updated_by'    => $sub_row->updated_by,
                            'updated_at'    => $sub_row->updated_at
                        );
                        array_push($object, $arr_sub_row);
                    }
                }
            }
            if($request->ajax()){
                return datatables()->of($object)->addIndexColumn()->toJson();
            }
            return view('menu.index')->with('active_menu', 'Menu');
        }else{
            return view('unauthorized');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $cek_url = M_menu::select('id')->where('url', $request->url)->first();
        if($cek_url != ''){
            return response()->json([
                'url'   => true
            ]);
        }else{
            $rel_menu = '0';
            if($request->s_rel != ''){
                $rel_menu = $request->s_rel;
            }
            $data = M_menu::create(
                [
                    'menu'          => $request->menu,
                    'url'           => $request->url,
                    'urutan'        => $request->urutan,
                    'rel'           => $rel_menu,
                    'created_by'    => $sess_username,
                    'created_at'    => date('Y-m-d H:i:s')
                ]
            );
            if($data){
                return response()->json([
                    'success'   => true,
                    'type'      => 'disimpan'
                ]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = M_menu::find($id);
        return response()->json([
            'id'        => $data->id,
            'menu'      => $data->menu,
            'url'       => $data->url,
            'urutan'    => $data->urutan,
            'rel'       => $data->rel
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $rel_menu = '0';
        if($request->s_rel != ''){
            $rel_menu = $request->s_rel;
        }
        $data = M_menu::find($id);
        $data->menu         = $request->menu;
        $data->url          = $request->url;
        $data->urutan       = $request->urutan;
        $data->rel          = $rel_menu;
        $data->updated_by   = $sess_username;
        $data->updated_at   = date('Y-m-d H:i:s');
        $data->save();

        return response()->json([
            'success'   => true,
            'type'      => 'diupdate'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function hapus(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $id = $request->id;
        $data = M_menu::find($id);
        $data->deleted_by   = $sess_username;
        $data->deleted_at   = date('Y-m-d H:i:s');
        $data->save();

        DB::table('tb_rel_group')
        ->where('id_group', $id)
        ->update([
            'deleted_by'   => $sess_username,
            'deleted_at'   => date('Y-m-d H:i:s')
        ]);
        // DB::table('tb_rel_group')->where('id_menu', $id)->delete();
        return response()->json([
            'success'   => true
        ]);
    }

    public function rel_menu(Request $request)
    {
        $data = M_menu::select(
            'id',
            'menu'
        )
        ->where('rel', '0')
        ->orderBy('urutan', 'asc')
        ->get();
        if($request->ajax()){
            return response()->json($data);
        }
    }
}
