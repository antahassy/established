<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use \App\Models\M_login;


class ID_Session
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!$request->session()->has('user.id')) {
            return Redirect("login"); 
        }
        $data = M_login::where("id", $request->session()->get('user.id'))->first();
        if(!$data)
        {
            $request->session()->flush();
            return Redirect("login");
            
        }
        return $next($request);
    }
}
