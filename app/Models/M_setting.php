<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_setting extends Model
{
    use HasFactory;
    protected $table = 'tb_setting';
    protected $guarded = [];
    protected $casts = [
	    'updated_at' => 'datetime:Y-m-d H:i:s'
	];
}
