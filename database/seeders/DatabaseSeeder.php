<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //deklarasikan seeder mana saja yang akan di eksekusi, baru 1 saja, tb_userSeeder.php
        $this->call([
        	tb_user_seeder::class,
            tb_group_seeder::class,
            tb_menu_seeder::class,
            tb_rel_group_seeder::class,
            tb_user_group_seeder::class,
            tb_setting_seeder::class
        ]);
    }
}
