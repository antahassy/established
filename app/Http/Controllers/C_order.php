<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use \App\Models\M_users;
use \App\Models\M_order;
use \App\Models\M_meja;
use \App\Models\M_food;

class C_order extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_id_group = DB::table('tb_user_group')->where('id_user', $sess_id)->first()->id_group;
        $akses = DB::table('tb_menu')
        ->select('tb_rel_group.akses')
        ->where('tb_menu.url', 'order')
        ->where('tb_menu.deleted_at', null)
        ->where('tb_rel_group.id_group', $sess_id_group)
        ->where('tb_rel_group.deleted_at', null)
        ->join('tb_rel_group', 'tb_menu.id', '=', 'tb_rel_group.id_menu')
        ->get();
        if(count($akses) != 0){
            $akses_temp = array();
            foreach ($akses as $key) {
                array_push($akses_temp, $key->akses);
            }
            if (in_array('1', $akses_temp)){
                $data = M_order::select(
                    'id',
                    'id_meja',
                    'order',
                    'status',
                    'biaya',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at'
                )
                ->where('deleted_at', null)
                ->orderBy('id', 'asc')
                ->get();
                foreach ($data as $row) {
                    $meja = DB::table('tb_meja')
                    ->select('tb_meja.nama')
                    ->where('tb_meja.deleted_at', null)
                    ->where('tb_meja.id', $row->id_meja)
                    ->first();
                    if($meja){
                        $row->meja = $meja->nama;
                    }else{
                        $row->meja = '';
                    }

                    $row->menu = DB::table('tb_order_detail')
                    ->select('tb_food.nama')
                    ->where('tb_order_detail.id_order', $row->id)
                    ->where('tb_order_detail.deleted_at', null)
                    ->join('tb_food', 'tb_order_detail.id_food', '=', 'tb_food.id')
                    ->get();

                    if($meja){
                        $row->meja = $meja->nama;
                    }else{
                        $row->meja = '';
                    }

                    if (in_array('3', $akses_temp) && in_array('4', $akses_temp)){
                        $row->action = '<button class="btn btn-sm btn-outline-success btn_edit" style="margin: 2.5px;">Edit</button>' . '<button class="btn btn-sm btn-outline-danger btn_delete" style="margin: 2.5px;">Hapus</button>';
                    }
                    if (! in_array('3', $akses_temp) && in_array('4', $akses_temp)){
                        $row->action = '<button class="btn btn-sm btn-outline-danger btn_delete" style="margin: 2.5px;">Hapus</button>';
                    }
                    if (in_array('3', $akses_temp) && ! in_array('4', $akses_temp)){
                        $row->action = '<button class="btn btn-sm btn-outline-success btn_edit" style="margin: 2.5px;">Edit</button>';
                    }
                    if (! in_array('3', $akses_temp) && ! in_array('4', $akses_temp)){
                        $row->action = '';
                    }
                }
                if($request->ajax()){
                    return datatables()->of($data)->addIndexColumn()->toJson();
                }
                return view('order.index')->with('active_menu', 'Order')->with('akses_menu', $akses_temp);
            }else{
                return view('unauthorized');
            }
        }else{
            return view('unauthorized');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $data = M_order::create(
            [
                'id_meja'       => $request->id_meja,
                'status'        => '1',
                'biaya'         => $request->total,
                'created_by'    => $sess_username,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => null
            ]
        );
        if($data){
            if(strlen($data->id) == 1){
                $order_number = 'ABC' . date('dmY') . '-00' . $data->id;
            }elseif(strlen($data->id) == 2){
                $order_number = 'ABC' . date('dmY') . '-0' . $data->id;
            }else{
                $order_number = 'ABC' . date('dmY') . '-' . $data->id;
            }
            $list_order = M_order::find($data->id);
            $list_order->order     = $order_number;
            $list_order->save();
            foreach ($request->list as $row) {
                $group = DB::table('tb_order_detail')->insert([
                    'id_order'      => $data->id,
                    'id_food'       => $row['id'],
                    'created_by'    => $sess_username,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => null
                ]);
            }
            return response()->json([
                'success'   => true,
                'type'      => 'disimpan',
                'xx'        => $data->id
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $detail = DB::table('tb_order_detail')
        ->orderBy('tb_order_detail.id_food', 'asc')
        ->select('tb_order_detail.id_food', 'tb_food.harga')
        ->where('tb_order_detail.id_order', $id)
        ->where('tb_order_detail.deleted_at', null)
        ->join('tb_food', 'tb_order_detail.id_food', '=', 'tb_food.id')
        ->get();
        foreach ($detail as $row) {
            $arr_detail[] = array(
                'id'        => strval($row->id_food),
                'price'     => (int)str_replace('.', '', $row->harga)
            );
        }
        $data = M_order::find($id);
        return response()->json([
            'id'        => $data->id,
            'id_meja'   => $data->id_meja,
            'order'     => $data->order,
            'detail'    => $arr_detail,
            'biaya'     => strval($data->biaya)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $data = M_order::find($id);
        $data->id_meja      = $request->id_meja;
        $data->biaya        = $request->total;
        $data->updated_by   = $sess_username;
        $data->updated_at   = date('Y-m-d H:i:s');
        $data->save();

        DB::table('tb_order_detail')->where('id_order', $id)->delete();
        foreach ($request->list as $row) {
            $group = DB::table('tb_order_detail')->insert([
                'id_order'      => $data->id,
                'id_food'       => $row['id'],
                'created_by'    => $sess_username,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => null
            ]);
        }

        return response()->json([
            'success'   => true,
            'type'      => 'diupdate'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function hapus(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $id = $request->id;
        $data = M_order::find($id);
        $data->deleted_by   = $sess_username;
        $data->deleted_at   = date('Y-m-d H:i:s');
        $data->save();

        return response()->json([
            'success'   => true
        ]);
    }

    public function s_meja(Request $request)
    {
        $data = M_meja::select(
            'id',
            'nama'
        )
        ->orderBy('id', 'asc')
        ->get();
        if($request->ajax()){
            return response()->json($data);
        }
    }

    public function s_menu(Request $request)
    {
        $data = M_food::select(
            'id',
            'nama',
            'image',
            'harga'
        )
        ->orderBy('id', 'asc')
        ->where('status', '1')
        ->get();
        if($request->ajax()){
            return response()->json($data);
        }
    }

    public function close_order(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $id = $request->id;
        $data = M_order::find($id);
        $data->status       = '0';
        $data->updated_by   = $sess_username;
        $data->updated_at   = date('Y-m-d H:i:s');
        $data->save();
        return response()->json([
            'success'   => true
        ]);
    }
}
