<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class tb_setting_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */ 
    public function run()
    {
        //multiple insert sesuai jumlah range data
    	// $faker = Faker::create();
    	// foreach (range(1,50) as $row) {
    	// 	DB::table('tb_user')->insert([
	    //     	'username' 			=> $faker->userName,
	    //     	'alamat' 			=> $faker->address,
	    //     	'tinggi'		 	=> $faker->randomDigit,
	    //     	'password'		 	=> \Hash::make($faker->userName),
	    //     	'created_at'		=> date('Y-m-d H:i:s'),
	    //     	'updated_at'		=> date('Y-m-d H:i:s')
	    //     ]);
    	// }

    	//single insert
        DB::table('tb_setting')->insert([
        	'meta_id' 			=> 'Judul',
        	'value' 			=> 'Bogor Cycling - Bike Organizer',
        	'updated_by'		=> 'System',
        	'updated_at'		=> date('Y-m-d H:i:s'),
        ]);
        DB::table('tb_setting')->insert([
        	'meta_id' 			=> 'Deskripsi',
        	'value' 			=> 'Manfaatkan Liburan Anda Bersama - sama Dengan Bersepeda Bersama Kami',
        	'updated_by'		=> 'System',
        	'updated_at'		=> date('Y-m-d H:i:s'),
        ]);
        DB::table('tb_setting')->insert([
        	'meta_id' 			=> 'Logo',
        	'value' 			=> '1618564033-logo-letter-5.png',
        	'updated_by'		=> 'System',
        	'updated_at'		=> date('Y-m-d H:i:s'),
        ]);
    }
}
