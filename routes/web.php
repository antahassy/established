<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\C_home;
use App\Http\Controllers\C_dashboard;
use App\Http\Controllers\C_login;
use App\Http\Controllers\C_group;
use App\Http\Controllers\C_menu;
use App\Http\Controllers\C_users;
use App\Http\Controllers\C_setting;
use App\Http\Controllers\C_order;
use App\Http\Controllers\C_food;
use App\Http\Controllers\C_meja;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome'); 
// });
Route::resource('/', C_dashboard::class);
//Diambil dari kernel
Route::group(['middleware' => 'id_session'], function() {
	Route::resource('dashboard', C_dashboard::class);

	Route::resource('users', C_users::class);
	Route::post('users/hapus', [C_users::class, 'hapus']);
	Route::post('users/aktifkan', [C_users::class, 'aktifkan']);
	Route::post('users/non_aktifkan', [C_users::class, 'non_aktifkan']);

	Route::resource('group', C_group::class);
	Route::post('group/hapus', [C_group::class, 'hapus']);
	Route::get('group/akses/{parameter}', [C_group::class, 'akses']);
	Route::get('group/sub_akses/{parameter}/{id}', [C_group::class, 'sub_akses']);
	Route::post('group/update_akses', [C_group::class, 'update_akses']);

	Route::resource('menu', C_menu::class);
	Route::post('menu/hapus', [C_menu::class, 'hapus']);
	Route::post('menu/rel_menu', [C_menu::class, 'rel_menu']);

	Route::resource('setting', C_setting::class);

	Route::get('login/logout', [C_login::class, 'logout']);

	Route::resource('order', C_order::class);
	Route::post('order/hapus', [C_order::class, 'hapus']);
	Route::post('order/s_meja', [C_order::class, 's_meja']);
	Route::post('order/s_menu', [C_order::class, 's_menu']);
	Route::post('order/close_order', [C_order::class, 'close_order']);

	Route::resource('feed', C_food::class);
	Route::post('feed/hapus', [C_food::class, 'hapus']);
	Route::post('feed/ready', [C_food::class, 'ready']);
	Route::post('feed/non_ready', [C_food::class, 'non_ready']);

	Route::resource('table', C_meja::class);
	Route::post('table/hapus', [C_meja::class, 'hapus']);
});
//Diambil dari kernel
Route::group(['middleware' => 'empty_session'], function() {
	Route::resource('login', C_login::class);
	Route::post('login/login', [C_login::class, 'login']);
});
