<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class tb_menu_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //multiple insert sesuai jumlah range data
    	// $faker = Faker::create();
    	// foreach (range(1,50) as $row) {
    	// 	DB::table('tb_user')->insert([
	    //     	'username' 			=> $faker->userName,
	    //     	'alamat' 			=> $faker->address,
	    //     	'tinggi'		 	=> $faker->randomDigit,
	    //     	'password'		 	=> \Hash::make($faker->userName),
	    //     	'created_at'		=> date('Y-m-d H:i:s'),
	    //     	'updated_at'		=> date('Y-m-d H:i:s')
	    //     ]);
    	// }

    	//single insert
        DB::table('tb_menu')->insert([
        	'menu' 				=> 'Dashboard',
        	'url'				=> 'dashboard',
        	'urutan'			=> '1',
        	'rel'				=> '0',
        	'created_by'		=> 'System',
        	'created_at'		=> date('Y-m-d H:i:s'),
        ]);
    }
}
