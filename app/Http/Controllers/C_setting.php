<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use \App\Models\M_users;
use \App\Models\M_setting;

class C_setting extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_id_group = DB::table('tb_user_group')->where('id_user', $sess_id)->first()->id_group;
        if($sess_id_group == '1'){
            $data = M_setting::all();
            if($request->ajax()){
                return response()->json($data);
            }
            return view('setting.index')->with('active_menu', 'Setting');
        }else{
            return view('unauthorized');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sess_id = Session::get('user.id');
        $sess_username = M_users::select('username')->where('id', $sess_id)->first()->username;

        $data_judul = M_setting::find('1');
        $data_judul->value        = $request->Judul;
        $data_judul->updated_by   = $sess_username;
        $data_judul->updated_at   = date('Y-m-d H:i:s');
        $data_judul->save();

        $data_deskripsi = M_setting::find('2');
        $data_deskripsi->value        = $request->Deskripsi;
        $data_deskripsi->updated_by   = $sess_username;
        $data_deskripsi->updated_at   = date('Y-m-d H:i:s');
        $data_deskripsi->save();

        $delete_berkas = $request->delete_berkas;
        $last_berkas = $request->get_berkas;

        $file_path = public_path('logo');
        $file_extension = array('jpg', 'jpeg', 'png', 'bmp', 'gif', 'svg', 'ico');
        $file_size = 5120000;//5 mb
        $file_upload = $request->file('berkas');
        if($file_upload != ''){
            if($file_upload->getSize() > $file_size){
                return response()->json([
                    'size'   => true
                ]);
            }else{
                if (in_array($file_upload->guessClientExtension(), $file_extension)){
                    $berkas = time() . '-' . $file_upload->getClientOriginalName();
                    $request->berkas->move($file_path, $berkas);
                    //update db
                    $data_logo = M_setting::find('3');
                    $data_logo->value        = $berkas;
                    $data_logo->updated_by   = $sess_username;
                    $data_logo->updated_at   = date('Y-m-d H:i:s');
                    $data_logo->save();

                    if($last_berkas != ''){
                        File::delete($file_path . '/' . $last_berkas);
                    }elseif($delete_berkas != ''){
                        File::delete($file_path . '/' . $delete_berkas);
                    }
                }else{
                    return response()->json([
                        'extension'   => true
                    ]);
                }
            }
        }else{
            if($last_berkas == '' && $delete_berkas != ''){
                File::delete($file_path . '/' . $delete_berkas);
                $berkas  = '';
                //update db
                $data_logo = M_setting::find('3');
                $data_logo->value        = $berkas;
                $data_logo->updated_by   = $sess_username;
                $data_logo->updated_at   = date('Y-m-d H:i:s');
                $data_logo->save();
            }elseif($last_berkas == '' && $delete_berkas == ''){
                $berkas  = '';
                //update db
                $data_logo = M_setting::find('3');
                $data_logo->value        = $berkas;
                $data_logo->updated_by   = $sess_username;
                $data_logo->updated_at   = date('Y-m-d H:i:s');
                $data_logo->save();
            }else{
                $berkas  = $last_berkas;
                //update db
                $data_logo = M_setting::find('3');
                $data_logo->value        = $berkas;
                $data_logo->updated_by   = $sess_username;
                $data_logo->updated_at   = date('Y-m-d H:i:s');
                $data_logo->save();
            }
        }

        return response()->json([
            'success'   => true
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
