@extends('layout/dashboard')
@section('content')
<style>
    .thead-dark tr th{
        color: #fff !important;
    }
    form label{
        margin-top: 5px;
        font-weight: 600 !important;
    }
</style>
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <?php
                    if(in_array('2', $akses_menu)){
                        echo '<h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">' . $active_menu . '</h5>';
                        echo '<div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>';
                        echo '<a href="#" class="btn btn-light-success font-weight-bolder btn-sm" id="btn_add">Tambah Data</a>';
                    }else{
                        echo '<h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">' . $active_menu . '</h5>';
                    }
                ?>
            </div>
            <div class="d-flex align-items-center">
                <!-- <a href="#" id="d_daily" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Daily</a>
                <a href="#" id="d_monthly" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Monthly</a>
                <a href="#" id="d_yearly" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">Yearly</a>
                <a href="#" class="btn btn-sm btn-light font-weight-bold mr-2" id="kt_dashboard_daterangepicker" data-toggle="tooltip" title="Select Daterange" data-placement="left">
                    <span class="text-muted font-size-base font-weight-bold mr-2" id="kt_dashboard_daterangepicker_title"></span>
                    <span class="text-primary font-size-base font-weight-bolder" id="kt_dashboard_daterangepicker_date"></span>
                </a> -->
            </div>
        </div>
    </div>
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row" style="min-height: 68vh;">
                <div class="col-lg-12 col-xxl-4 order-1 order-xxl-2">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-header border-0">
                            <h3 class="card-title font-weight-bolder text-dark">Daftar {{ $active_menu }}</h3>
                        </div>
                        <div class="card-body pt-0">
                            <table id="table_data" class="table table-striped table-vcenter" style="width: 100%;">
                                <thead class="thead-dark">
                                    <tr> 
                                        <th>No</th>
                                        <th>Kategori</th>
                                        <th>Nama</th> 
                                        <th>Harga</th> 
                                        <th>Status</th> 
                                        <th>Tindakan</th> 
                                        <th>Dibuat</th> 
                                        <th>Diupdate</th> 
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal animated" id="modal_form" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
            </div>
            <div class="modal-body"> 
                <form id="form_data">
                    <input type="hidden" id="id_data" name="id_data">
                    <input type="hidden" name="_method" id="_method">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>Kategori Menu<span style="color: red;">*</span></label>
                            <select class="form-control" name="s_kategori" id="s_kategori">
                                <option value="">Pilih Kategori</option>
                                <option value="1">Makanan</option>
                                <option value="2">Minuman</option>
                            </select>

                            <label>Nama Menu<span style="color: red;">*</span></label>
                            <input type="text" name="nama" id="nama" class="form-control">

                            <label>Harga Menu<span style="color: red;">*</span></label>
                            <input type="text" name="harga" id="harga" class="form-control">

                            <label>Gambar Menu<!-- <span style="color: red;">*</span> --></label>
                            <input type="hidden" name="delete_berkas" id="delete_berkas" value="">
                            <input type="hidden" name="get_berkas" id="get_berkas" value="">
                            <input type="file" name="berkas" id="berkas" accept="image/*" style="width: 100%;">
                            <div>
                                <div id="delete_preview_items">Hapus Gambar</div>
                                <img id="preview_items" src="" title="">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-outline-primary" id="btn_process"></button>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready(function(){
        $.ajaxSetup({
            headers     : {
                'X-CSRF-TOKEN'  : $('meta[name="csrf-token"]').attr('content')
            }
        });
        var nama_bulan = new Array();
        nama_bulan[1] = 'Januari';
        nama_bulan[2] = 'Februari';
        nama_bulan[3] = 'Maret';
        nama_bulan[4] = 'April';
        nama_bulan[5] = 'Mei';
        nama_bulan[6] = 'Juni';
        nama_bulan[7] = 'Juli';
        nama_bulan[8] = 'Agustus'; 
        nama_bulan[9] = 'September'; 
        nama_bulan[10] = 'Oktober';
        nama_bulan[11] = 'November'; 
        nama_bulan[12] = 'Desember';
        var table_data = '';
        swal({
            showConfirmButton   : false,
            allowOutsideClick   : false,
            allowEscapeKey      : false,
            background          : 'transparent',
            onOpen  : function(){
                swal.showLoading();
                setTimeout(function(){
                    data_table();
                },500);
            }
        });
        function main(){
            var modal_form;
            $('#modal_form').on('show.bs.modal', function(){
                $(this).addClass('zoomIn');
                modal_form = true;
            });
            $('#modal_form').on('hide.bs.modal', function(){
                if(modal_form){
                    $(this).removeClass('zoomIn').addClass('zoomOut');
                    modal_form = false;
                    setTimeout(function(){
                        $('#modal_form').modal('hide');
                    },350);
                    return false;
                }
                $(this).removeClass('zoomOut');
            });
            $('#harga').on('keyup', function(){
                $(this).val(function(index, value) {
                    return value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                });
            });
            $('#btn_add').on('click', function(){
                $('#form_data')[0].reset();
                $('#modal_form').find('.modal-title').text('Tambah');
                $('#btn_process').text('Simpan');
                $('#form_data').attr('method', 'post');
                $('#_method').val('post');
                $('#form_data').attr('action', site + '/feed');
                $('#delete_preview_items').css('display','none');
                $('#preview_items').attr('src', '');
                $('#berkas, #get_berkas, #delete_berkas').val('');
                $('#modal_form').modal('show');
                display_image();
                data_process();
            });
            $('#table_data').on('click', '.btn_edit', function(){
                var action_data = table_data.row($(this).parents('tr')).data();
                swal({
                    showConfirmButton   : false,
                    allowOutsideClick   : false,
                    allowEscapeKey      : false,
                    background          : 'transparent',
                    onOpen  : function(){
                        swal.showLoading();
                        setTimeout(function(){
                            $.ajax({
                                type           : 'ajax',
                                method         : 'get',
                                url            : site + '/feed/' + action_data.id + '/edit',
                                async          : true,
                                dataType       : 'json',
                                success        : function(data){
                                    $('#form_data')[0].reset();
                                    $('#s_kategori').val(data.kategori);
                                    $('#nama').val(data.nama);
                                    $('#harga').val(data.harga);

                                    if(! data.image){
                                        $('#get_berkas, #delete_berkas').val('');
                                        $('#preview_items').attr('src', site + '/metronic/media/users/blank.png');
                                        $('#delete_preview_items').hide();
                                    }else{
                                        $('#get_berkas, #delete_berkas').val(data.image);
                                        $('#preview_items').attr('src', site + '/food/' + data.image);
                                        $('#delete_preview_items').show();
                                    }
                                    $('#berkas').val('');
                                    display_image(); 

                                    $('#modal_form').find('.modal-title').text('Edit');
                                    $('#btn_process').text('Update');
                                    $('#form_data').attr('method', 'post');
                                    $('#_method').val('put');
                                    $('#form_data').attr('action', site + '/feed/' + data.id);
                                    swal.close();
                                    $('#modal_form').modal('show');
                                    data_process();
                                },
                                error   : function(){
                                    swal({
                                        background  : 'transparent',
                                        html        : '<pre>Koneksi terputus' + '<br>' + 
                                                      'Cobalah beberapa saat lagi</pre>',
                                        type        : "warning"
                                    });
                                }
                            });
                        },500);
                    }
                });     
            });
            $('#table_data').on('click', '#btn_ready', function(){
                var id = $(this).attr('data');
                var food_status = $(this).attr('alt');
                var question = '', link_url = '';
                if(food_status == '0'){
                    question = 'Ready ?';
                    link_url = site + '/feed/ready';
                }else{
                    question = 'Non Ready ?';
                    link_url = site + '/feed/non_ready';
                }
                swal({
                    html                : '<pre>' + question + '</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'Tidak',
                    confirmButtonText   : 'Ya'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        data        : {id : id},
                                        url         : link_url,
                                        dataType    : "json",
                                        async       : true,
                                        success: function(response){
                                            if(response.success){
                                                swal.close();
                                                setTimeout(function(){
                                                    table_data.ajax.reload();
                                                },500);
                                            }
                                        },
                                        error: function (){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Koneksi terputus' + '<br>' + 
                                                              'Cobalah beberapa saat lagi</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            });
            $('#table_data').on('click', '.btn_delete', function(){
                var action_data = table_data.row($(this).parents('tr')).data();
                swal({
                    html                : '<pre>Keterangan meja' + '<br>' + 
                                          'Pada menu order akan hilang' + '<br>' + 
                                          'Hapus data ini ?</pre>',
                    type                : "question",
                    background          : 'transparent',
                    showCancelButton    : true,
                    cancelButtonText    : 'Tidak',
                    confirmButtonText   : 'Ya'
                }).then((result) => {
                    if(result.value){
                        swal({
                            showConfirmButton   : false,
                            allowOutsideClick   : false,
                            allowEscapeKey      : false,
                            background          : 'transparent',
                            onOpen  : function(){
                                swal.showLoading();
                                setTimeout(function(){
                                    $.ajax({
                                        type        : 'ajax',
                                        method      : 'post',
                                        data        : {id : action_data.id},
                                        url         : site + '/feed/hapus',
                                        dataType    : "json",
                                        async       : true,
                                        success   : function(response){
                                            if(response.success){
                                                swal({
                                                    html                : '<pre>Data berhasil dihapus</pre>',
                                                    type                : "success",
                                                    background          : 'transparent',
                                                    allowOutsideClick   : false,
                                                    allowEscapeKey      : false, 
                                                    showConfirmButton   : false,
                                                    timer               : 1000
                                                }).then(function(){
                                                    setTimeout(function(){
                                                        table_data.ajax.reload();
                                                    },500);
                                                });
                                            }
                                        },
                                        error          : function(){
                                            swal({
                                                background  : 'transparent',
                                                html        : '<pre>Koneksi terputus' + '<br>' +
                                                              'Cobalah beberapa saat lagi</pre>',
                                                type        : "warning"
                                            });
                                        }
                                    });
                                },500);
                            }
                        });
                    }
                });
            });
        }
        function display_image(){
            function preview(image){
                if(image.files && image.files[0]){
                    var reader      = new FileReader();
                    reader.onload   = function(event){
                        $('#preview_items').attr('src', event.target.result);
                        $('#preview_items').attr('title', image.files[0].name);
                        $('#delete_preview_items').css('display','block');
                    }
                    reader.readAsDataURL(image.files[0]);
                }
            }
            $("#berkas").on('change', function(){
                preview(this);
                var names = $(this).val();
                var file_names = names.replace(/^.*\\/, "");
            });
            $('#delete_preview_items').on('click', function(){
                $('#delete_preview_items').css('display','none');
                $('#preview_items').attr('src', '');
                $('#berkas, #get_berkas').val('');
            });
        }
        function data_process(){
            $('#btn_process').on('click', function(event){
                event.preventDefault();
                event.stopImmediatePropagation();
                var ajax_method     = $('#form_data').attr('method');
                var ajax_url        = $('#form_data').attr('action');
                var form_data       = $('#form_data')[0];
                form_data           = new FormData(form_data);
                var errormessage    = '';
                if(! $('#s_kategori').val()){
                     errormessage += 'Kategori menu dibutuhkan \n';
                }
                if(! $('#nama').val()){
                     errormessage += 'Nama menu dibutuhkan \n';
                }
                if(! $('#harga').val()){
                     errormessage += 'Harga menu dibutuhkan \n';
                }
                if(errormessage !== ''){
                    swal({
                        background  : 'transparent',
                        html        : '<pre>' + errormessage + '</pre>'
                    });
                }else{
                    swal({
                        background          : 'transparent',
                        html                : '<pre>Apakah data sudah benar ?</pre>',
                        type                : 'question',
                        showCancelButton    : true,
                        cancelButtonText    : 'Tidak',
                        confirmButtonText   : 'Ya'
                    }).then((result) => {
                        if(result.value){
                            swal({
                                showConfirmButton   : false,
                                allowOutsideClick   : false,
                                allowEscapeKey      : false,
                                background          : 'transparent',
                                onOpen  : function(){
                                    swal.showLoading();
                                    setTimeout(function(){
                                        $.ajax({
                                            type           : 'ajax',
                                            method         : ajax_method,
                                            url            : ajax_url,
                                            data           : form_data,
                                            async          : true,
                                            processData    : false,
                                            contentType    : false,
                                            cache          : false,
                                            dataType       : 'json',
                                            success        : function(response){
                                                if(response.success){
                                                    $('#modal_form').modal('hide');
                                                    $('#form_data')[0].reset();
                                                    swal({
                                                        html                : '<pre>Data berhasil ' + response.type + '</pre>',
                                                        type                : "success",
                                                        background          : 'transparent',
                                                        allowOutsideClick   : false,
                                                        allowEscapeKey      : false, 
                                                        showConfirmButton   : false,
                                                        timer               : 1000
                                                    }).then(function(){
                                                        setTimeout(function(){
                                                            table_data.ajax.reload();
                                                        },500);
                                                    });
                                                    if(response.size){
                                                        swal({
                                                            html                : '<pre>Ukuran file terlalu tinggi' + '<br>' + 
                                                                                  'Maks 5 mb</pre>',
                                                            type                : "warning",
                                                            background          : 'transparent'
                                                        });
                                                    }
                                                    if(response.extension){
                                                        swal({
                                                            html                : '<pre>Ekstensi file tidak sesuai</pre>',
                                                            type                : "warning",
                                                            background          : 'transparent'
                                                        });
                                                    }
                                                }
                                            },
                                            error   : function(){
                                                swal({
                                                    background  : 'transparent',
                                                    html        : '<pre>Koneksi terputus' + '<br>' + 
                                                                  'Cobalah beberapa saat lagi</pre>',
                                                    type        : "warning"
                                                });
                                            }
                                        });
                                    },500);
                                }
                            });     
                        }
                    });
                }
                return false;
            });
        }
        function data_table(){
            table_data = $('#table_data').DataTable({
                lengthMenu          : [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
                processing          : true, 
                destroy             : true,
                serverSide          : true, 
                scrollX             : true,
                scrollCollapse      : true,
                fixedColumns        : true, 
                initComplete: function(){
                    swal.close();
                    main();
                },
                ajax            : {
                    url         : site + '/feed',
                    method      : 'get'
                },
                columns             : [
                    {data   : 'DT_RowIndex'},
                    {data   : 'kategori',
                        render: function(kategori){
                            if(kategori == '1'){
                                return 'Makanan';
                            }
                            if(kategori == '2'){
                                return 'Minuman';   
                            }else{
                                return '';
                            }
                        }
                    },
                    {data   : 'nama',
                        render         : (data, type, row) => {
                            if(row.image != null && row.image != ''){
                                return '<img src="' + site + '/food/' + row.image + '" style="width: 125px; height: 75px;">' + '<br>' + '<div style="text-align: center; padding-top: 5px; font-weight: 800;">' + row.nama + '</div>';
                            }else{
                                return row.nama;
                            }
                        }
                    },
                    {data   : 'harga',
                        render: function(harga){
                            return 'Rp. ' + harga;
                        }
                    },
                    {data           : 'status',
                        render         : (data, type, row) => {
                            if(row.status == '1'){
                                return '<button id="btn_ready" data="' + row.id + '" alt="' + row.status + '" class="btn btn-sm btn-rounded btn-outline-primary">Ready</button>';
                            }else{
                                return '<button id="btn_ready" data="' + row.id + '" alt="' + row.status + '" class="btn btn-sm btn-rounded btn-outline-danger">Non Ready</button>';
                            }
                        }
                    },
                    {data   : 'action'},
                    {data   : 'created_at',
                        render: function(created_at){
                            var time = created_at.split(' ');
                            return time[0].split('-')[2] + '/' + nama_bulan[Number(time[0].split('-')[1])] + '/' + time[0].split('-')[0] + '<br>' + time[1]; 
                        }
                    },
                    {data   : 'updated_at',
                        render: function(updated_at){
                            if(updated_at != null){
                                var time = updated_at.split(' ');
                                return time[0].split('-')[2] + '/' + nama_bulan[Number(time[0].split('-')[1])] + '/' + time[0].split('-')[0] + '<br>' + time[1]; 
                            }else{
                                return '';
                            }
                        }
                    }
                ]
            });
        }
    })
</script>
@endsection