<?php
    $meta = DB::table('tb_setting')->get();
?>
<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
    <title>{{ $meta[0]->value }}</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <meta name="description" content="{{ $meta[0]->value }}. {{ $meta[1]->value }}">
    <meta name="keywords" content="Bogor,Cycling,Sepeda,Gunung,Touring,Bicycle,Ride,Trip,Journey,Wisata,Bike,Organizer">
    <meta name="googlebot-news" content="index,follow">
    <meta name="googlebot" content="index,follow">
    <meta name="author" content="Antahassy Wibawa">
    <meta name="robots" content="index,follow">
    <meta name="language" content="id">
    <meta name="Classification" content="Sport">
    <meta name="geo.country" content="Indonesia">
    <meta name="geo.placename" content="Indonesia"> 
    <meta name="geo.position" content="-6.5899176; 106.8230479">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="content-language" content="In-Id">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="Copyright" content="{{ $meta[0]->value }}">
    <meta property="og:title" content="{{ $meta[0]->value }}">
    <meta property="og:url" content="http://127.0.0.1:8000/">
    <meta property="og:type" content="Sport">
    <meta property="og:site_name" content="{{ $meta[0]->value }}">
    <meta itemprop="name" content="{{ $meta[0]->value }}">

    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('logo/' . $meta[2]->value . '?t=').mt_rand() }}">

    <link rel="stylesheet" href="{{ asset('travel/css/bootstrap.min.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('travel/css/owl.carousel.min.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('travel/css/magnific-popup.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('travel/css/font-awesome.min.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('travel/css/themify-icons.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('travel/css/nice-select.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('travel/css/flaticon.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('travel/css/gijgo.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('travel/css/animate.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('travel/css/slick.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('travel/css/slicknav.css?t=').mt_rand() }}">
    <link rel="stylesheet" href="{{ asset('travel/css/jquery-ui2.css?t=').mt_rand() }}">

    <link rel="stylesheet" href="{{ asset('travel/css/style.css?t=').mt_rand() }}">

    <script src="{{ asset('travel/js/vendor/modernizr-3.5.0.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/vendor/jquery-1.12.4.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/popper.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/bootstrap.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/owl.carousel.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/isotope.pkgd.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/ajax-form.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/waypoints.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/jquery.counterup.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/imagesloaded.pkgd.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/scrollIt.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/jquery.scrollUp.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/wow.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/nice-select.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/jquery.slicknav.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/jquery.magnific-popup.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/plugins.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/gijgo.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/slick.min.js?t=').mt_rand() }}"></script>
   
    <script src="{{ asset('travel/js/contact.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/jquery.ajaxchimp.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/jquery.form.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/jquery.validate.min.js?t=').mt_rand() }}"></script>
    <script src="{{ asset('travel/js/mail-script.js?t=').mt_rand() }}"></script>


    <script src="{{ asset('travel/js/main.js?t=').mt_rand() }}"></script>
    <script>
        $('#datepicker').datepicker({
            iconsLibrary: 'fontawesome',
            icons: {
             rightIcon: '<span class="fa fa-caret-down"></span>'
         }
        });
    </script>
</head>
<body>
    <header>
        <div class="header-area ">
            <div id="sticky-header" class="main-header-area">
                <div class="container-fluid">
                    <div class="header_bottom_border">
                        <div class="row align-items-center">
                            <div class="col-xl-2 col-lg-2">
                                <div class="logo">
                                    <a href="index.html">
                                        <img src="{{ asset('travel/img/logo.png?t=').mt_rand() }}" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6">
                                <div class="main-menu  d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="index.html">home</a></li>
                                            <li><a href="about.html">About</a></li>
                                            <li><a class="" href="travel_destination.html">Destination</a></l/li>
                                            <li><a href="#">pages <i class="ti-angle-down"></i></a>
                                                <ul class="submenu">
                                                        <li><a href="destination_details.html">Destinations details</a></li>
                                                        <li><a href="elements.html">elements</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">blog <i class="ti-angle-down"></i></a>
                                                <ul class="submenu">
                                                    <li><a href="blog.html">blog</a></li>
                                                    <li><a href="single-blog.html">single-blog</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="contact.html">Contact</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-xl-4 col-lg-4 d-none d-lg-block">
                                <div class="social_wrap d-flex align-items-center justify-content-end">
                                    <div class="number">
                                        <p> <i class="fa fa-phone"></i> 10(256)-928 256</p>
                                    </div>
                                    <div class="social_links d-none d-xl-block">
                                        <ul>
                                            <li><a href="#"> <i class="fa fa-instagram"></i> </a></li>
                                            <li><a href="#"> <i class="fa fa-linkedin"></i> </a></li>
                                            <li><a href="#"> <i class="fa fa-facebook"></i> </a></li>
                                            <li><a href="#"> <i class="fa fa-google-plus"></i> </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="seach_icon">
                                <a data-toggle="modal" data-target="#exampleModalCenter" href="#">
                                    <i class="fa fa-search"></i>
                                </a>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
	@yield('website')
    <footer class="footer">
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-md-6 col-lg-4 ">
                        <div class="footer_widget">
                            <div class="footer_logo">
                                <a href="#">
                                    <img src="{{ asset('travel/img/footer_logo.png?t=').mt_rand() }}" alt="">
                                </a>
                            </div>
                            <p>5th flora, 700/D kings road, green <br> lane New York-1782 <br>
                                <a href="#">+10 367 826 2567</a> <br>
                                <a href="#">contact@carpenter.com</a>
                            </p>
                            <div class="socail_links">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <i class="ti-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="ti-twitter-alt"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-pinterest"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-youtube-play"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-md-6 col-lg-2">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Company
                            </h3>
                            <ul class="links">
                                <li><a href="#">Pricing</a></li>
                                <li><a href="#">About</a></li>
                                <li><a href="#"> Gallery</a></li>
                                <li><a href="#"> Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Popular destination
                            </h3>
                            <ul class="links double_links">
                                <li><a href="#">Indonesia</a></li>
                                <li><a href="#">America</a></li>
                                <li><a href="#">India</a></li>
                                <li><a href="#">Switzerland</a></li>
                                <li><a href="#">Italy</a></li>
                                <li><a href="#">Canada</a></li>
                                <li><a href="#">Franch</a></li>
                                <li><a href="#">England</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-lg-3">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Instagram
                            </h3>
                            <div class="instagram_feed">
                                <div class="single_insta">
                                    <a href="#">
                                        <img src="{{ asset('travel/img/instagram/1.png?t=').mt_rand() }}" alt="">
                                    </a>
                                </div>
                                <div class="single_insta">
                                    <a href="#">
                                        <img src="{{ asset('travel/img/instagram/2.png?t=').mt_rand() }}" alt="">
                                    </a>
                                </div>
                                <div class="single_insta">
                                    <a href="#">
                                        <img src="{{ asset('travel/img/instagram/3.png?t=').mt_rand() }}" alt="">
                                    </a>
                                </div>
                                <div class="single_insta">
                                    <a href="#">
                                        <img src="{{ asset('travel/img/instagram/4.png?t=').mt_rand() }}" alt="">
                                    </a>
                                </div>
                                <div class="single_insta">
                                    <a href="#">
                                        <img src="{{ asset('travel/img/instagram/5.png?t=').mt_rand() }}" alt="">
                                    </a>
                                </div>
                                <div class="single_insta">
                                    <a href="#">
                                        <img src="{{ asset('travel/img/instagram/6.png?t=').mt_rand() }}" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="modal fade custom_search_pop" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    	<div class="modal-dialog modal-dialog-centered" role="document">
      		<div class="modal-content">
        		<div class="serch_form">
            		<input type="text" placeholder="Search" >
            		<button type="submit">search</button>
        		</div>	
      		</div>
    	</div>
    </div>
</body>
</html>